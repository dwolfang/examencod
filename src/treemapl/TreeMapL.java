/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package treemapl;
//Improtamos los paquetes necesarios para el programa.
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author Dh.wolfang
 */
public class TreeMapL {
//Se declaran los atributos para crear los get y los Set.
private String nombre;
private int precio;

    public static void main(String[] args) {
       
      // Definimos el TreeMap
TreeMap lista = new TreeMap(); 
System.out.println("Esta es nuestra lista de productos");
// Agregamos la "clave"-"valor" al HashMap 
// En la primera lista ponemos los codigos y los productos disponibles
lista.put("3250", "Zapatos"); 
lista.put("3255","Bolsos"); 
lista.put("3256", "Camisas Hombre"); 
lista.put("3257", "Camisas Mujer"); 
lista.put("3258", "Abrigos"); 

mostrarMapa(lista); //Muestra la lista
System.out.println("Esta es nuestra lista de precios");
//Creamos una segunda lista con el ID del producto y el precio correspondiente.
TreeMap listap = new TreeMap();
listap.put("3250","25 €");
listap.put("3255","15 €");
listap.put("3256","19,90 €");
listap.put("3257","19,90 €");
listap.put("3258","39,90 €");
//Mostramos de nuevo la lista
mostrarMapa(listap);

       

} 

public static void mostrarMapa(Map lista) {
 //Creamos un Iterator para recorrer la lista
for( Iterator it = lista.keySet().iterator(); it.hasNext();) {
String clave = (String)it.next();
String valor = (String)lista.get(clave);

System.out.println(clave + " : " + valor);


   

}
}

    /**
     * @return  nombre producto
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return el precio del producto
     */
    public int getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(int precio) {
        this.precio = precio;
    }

    }

    

